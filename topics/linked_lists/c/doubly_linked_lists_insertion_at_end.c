void insert_end(struct Node ** head, int new_data) {
  struct Node * newNode = new Node;
  struct Node * last = NULL;

  try {
    newNode = new Node;
    newNode -> data = new_data;
    newNode -> next = NULL;

  } catch (...) {
    cout << "No hay suficiente memoria";
    exit(0);
  }

  last = * head;

  if ( * head == NULL) {
    newNode -> prev = NULL;
    * head = newNode;
  } else {
    while (last -> next != NULL)
      last = last -> next;
  }

  last -> next = newNode;
  newNode -> prev = last;

}