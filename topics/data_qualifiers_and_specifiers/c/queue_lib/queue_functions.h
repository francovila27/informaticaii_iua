#ifndef QUEUE_FUNCTIONS_H
#define QUEUE_FUNCTIONS_H 

struct Node;

void push      (struct Node **, int);
void append    (struct Node **, int);
void print_list(struct Node*);

#endif

