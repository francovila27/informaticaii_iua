/*Verificamos que la pila no este vacia*/
if (stackptr != NULL) 
{
    /*Asignamos en temp el stack pointer actual*/
    temp = stackptr;
    /*Asignamos al stack pointer, el valor siguiente
    del primer nodo*/
    stackptr = stackptr->siguiente;
    /*Liberamos la memoria ocupada por el primer nodo*/
    free(temp);
}
else
{
    printf("Pila vacia\n");
}